from models import Game
import sys

board_size = int(sys.argv[1]) if len(sys.argv) > 1 else 4
save_name = sys.argv[2] if len(sys.argv) > 2 else 'save'

if __name__ == '__main__':
    game = Game(board_size, save_name)
    game.play()
