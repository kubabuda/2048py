import time
from random import randint
import utils


class Game(object):
    '''Class for console 2048 game
    '''
    def __init__(self, board_size=4, save_name='save'):
        '''Tries to load the game state form save file
        '''
        self.size = board_size      # game can be called with custom size field
        if save_name == 'save':
            save_name = ''.join([save_name, str(self.size)])
        self.save_name = save_name

        save = utils.read_save(self.save_name)
        if save.size == self.size:
            self.board = save.board[:]
            self.score = save.score
            self.hiscore = save.hiscore
        else:                       # saved game had different grid dimensions
            self.board = [[0 for column in range(self.size)]
                          for row in range(self.size)]
            self.score = 0
            self.hiscore = 0

        super(Game, self).__init__()

    def show_on_stdout(self, board=None):
        '''Prints game board on standard output.
        '''
        if not board:
            board = self.board
        utils.clear_screen()
        if self.score > self.hiscore:
            self.hiscore = self.score
        print 'SCORE:%d   HISCORE:%d' % (self.score, self.hiscore)

        print '+' + '----+' * self.size
        for row in board:
            line = [utils.justify(piece) for piece in row]
            print ''.join(line) + '|'
            print '+' + '----+' * self.size
        print

    def clear(self):
        '''Clears board an current score for new game
        '''
        self.board = [[0 for column in range(self.size)]
                      for row in range(self.size)]
        self.score = 0

    def insert(self):
        '''Inserts value: 2 or sometimes 4
        in (pseudo)randomly chosen empty field on the board.
        '''
        is_set = False
        counter = 0
        max_counter = self.size ** 2

        while not is_set and counter < max_counter:
            x = randint(0, 100) % self.size
            y = randint(0, 100) % self.size

            if not self.board[y][x]:
                val = 4 if randint(0, 10) > 7 else 2
                self.board[y][x] = val
                is_set = True
            else:
                ++counter
        if counter >= max_counter:
            raise IOError('Too much tries')

    def slide(self):
        '''Slides pieces on board to the left,
        adding clashing pieces with the same value
        '''
        moved = False
        for row_no in range(self.size):
            try:
                row = self.board[row_no]
            except IndexError:
                print '1. INDEX ERROR AT:', row_no
                quit()
        # slide moves anything only if theres non-0 value after first 0
            if 0 in row and [i for i in row[row.index(0):] if i != 0]:
                row = [i for i in row if i != 0]
                if row:
                    moved = True

            for i in range(len(row) - 1):
                if row[i] == row[i+1] and row[i]:
                    row[i] *= 2
                    row[i+1] = 0
                    self.score += row[i]
                    moved = True

            if 0 in row:
                row = [i for i in row if i != 0]
            row.extend([0] * (self.size - len(row)))

            try:
                self.board[row_no] = row[:]
            except IndexError:
                print '2. INDEX ERROR AT:', row_no
                quit()

        return moved

    def rotate(self, n=1):
        '''Rotates game board 90* clockwise, n times (one by default)
        '''
        for i in range(n):                      # rotate board n times
            new_board = zip(*self.board[::-1])  # cool wizardry B)
            self.board = [[i for i in row] for row in new_board]

    def can_play(self):
        '''Checks whether player can do any move, returns True or False
        '''
        for row in self.board:
            if 0 in row:
                return True

        for row in self.board:
            for i in range(self.size - 1):
                if row[i] == row[i+1]:
                    return True

        for column in zip(*self.board[::-1]):
            for i in range(self.size - 1):
                if column[i] == column[i+1]:
                    return True

        return False

    def take_action(self):
        '''Asks player for action, and tries to execute it
        '''
        action = None
        moved_anything = False
        while not moved_anything:
            while not action:
                try:
                    action = raw_input('> ').lower()
                except KeyError:
                    continue
                except (EOFError, KeyboardInterrupt):
                    self.exit()

            if 'w' in action:       # Up move
                self.rotate(3)
                moved_anything = self.slide()
                self.rotate()
            elif 's' in action:     # Move down
                self.rotate()
                moved_anything = self.slide()
                self.rotate(3)
            elif 'd' in action:     # To the right
                self.rotate(2)
                moved_anything = self.slide()
                self.rotate(2)
            elif 'a' in action:     # Left side
                moved_anything = self.slide()
            elif 'q' in action or 'e' in action or 'x' in action:
                self.exit()
            elif 'r' in action:
                if self.next_game('RESTART'):
                    return True
            self.show_on_stdout()
            action = False

        time.sleep(0.25)

    def exit(self, message='QUIT'):
        '''Asks whether player really wants to quit, calls self.quit if so,
        returns False otherwise
        '''
        question = ''.join([message, '? (Y/N):'])
        try:
            usr_input = raw_input(question).lower()
        except (EOFError, KeyboardInterrupt):
            self.quit()
        if 'y' in usr_input or 'q' in usr_input:
            self.quit()
        else:
            return False

    def quit(self):
        '''Saves game and quits
        '''
        utils.clear_screen()
        self.checskum = utils.calculate_checksum(self)
        utils.save_game(self, self.save_name)
        quit()

    def next_game(self, message='QUIT'):
        '''After losing game or hiting restart,
        asks player whether he wants to start new one,
        returns True or False
        '''
        question = ''.join([message, '? (Y/N):'])
        try:
            usr_input = raw_input(question).lower()
            if 'y' in usr_input:
                self.clear()
                return True
            else:
                return False
        except (EOFError, KeyboardInterrupt):
            self.quit()

    def new_game(self):
        '''Launches new game with current Game.board
        '''
        # clear board if saved game was lost
        if not self.can_play():
            self.clear()
        # insert one dice if game board is fully empty
        empty_rows = [all(i == 0 for i in row) for row in self.board]
        if all(empty_row is True for empty_row in empty_rows):
            self.insert()

        while self.can_play():
            self.show_on_stdout()
            self.take_action()
            self.insert()
        else:
            self.show_on_stdout()
            print 'GAME OVER, SCORE: %d' % self.score
            utils.save_game(self, self.save_name)

    def play(self):
        self.new_game()
        while self.next_game('NEW GAME'):
            self.new_game()
