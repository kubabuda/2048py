import os


class Save(object):
    """Save for game, has all attributes necessary to restore saved game state
    """
    def __init__(self):
        self.size = 4
        self.score = 0
        self.hiscore = 0
        self.board = [[0 for column in xrange(self.size)]
                      for row in xrange(self.size)]

    def clear(self):
        self.__init__()


def clear_screen():
    '''Clears console in system specific way
    '''
    if os.name == 'nt':
        os.system('cls')
    elif os.name == 'posix':
        os.system('clear')


def justify(n):
    '''Takes integer from board, returns it to printing justified with spaces
    '''
    if n >= 10000:
        raise KeyError('No human can get this far B)')

    if not n:
        return '|    '
    elif n < 10:
        return '|  %d ' % n
    elif n < 100:
        return '| %d ' % n
    elif n < 1000:
        return '| %d' % n
    else:
        return '|%d' % n


def calculate_checksum(game):
    '''Calculates simple checksum for given game, to prevent save editing
    '''
    checksum = sum([sum(row) for row in game.board]) + game.size + game.score
    return checksum * game.hiscore


def save_game(game, save_name='save'):
    '''Saves game state in save_name file
    '''
    with open(save_name, 'w') as save:
        checksum = calculate_checksum(game)
        save.write(('%d %d %d %d\n') %
                   (game.size, game.score, game.hiscore, checksum))
        for row in game.board:
            save.write(' '.join([str(i) for i in row]))
            save.write('\n')


def read_save(save_name='save'):
    '''Reads game state from save_name file
    '''
    save = Save()
    try:
        with open(save_name, 'r') as prev_save:
            prev_save = prev_save.readlines()
            prev_state = [[int(i) for i in line.split()]
                          for line in prev_save]

            save.size, save.score, save.hiscore, save.checksum = prev_state[0]
            save.board = [[i for i in row] for row in prev_state[1:]]

            if not all(len(line) == len(save.board) for line in save.board):
                raise IndexError
            if save.checksum != calculate_checksum(save):
                print 'Invalid checksum, should be', calculate_checksum(save)
                raise IndexError
            else:
                return save

    except IOError:
        print 'NO SAVED GAME'
    except IndexError:
        print 'SAVE CORRUPTED'

    save = Save()
    return save
